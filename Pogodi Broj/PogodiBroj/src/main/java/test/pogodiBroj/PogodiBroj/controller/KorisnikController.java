package test.pogodiBroj.PogodiBroj.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties.Headers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import test.pogodiBroj.PogodiBroj.model.Korisnik;
import test.pogodiBroj.PogodiBroj.service.KorisnikService;

@RestController
public class KorisnikController {

	@Autowired
	KorisnikService korisnikService;

	@CrossOrigin
	@RequestMapping(value = "api/korisnici", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Korisnik>> preuzmiKorisnike() {
		List<Korisnik> korisnici = korisnikService.findAll();

		return new ResponseEntity<>(korisnici, HttpStatus.OK);
	}
	@CrossOrigin
	@RequestMapping(value = "api/korisnici", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<Korisnik> snimiKorisnika(@RequestBody Korisnik korisnik) {
		Korisnik noviKorisnik = new Korisnik();
		noviKorisnik.setIme(korisnik.getIme());
		noviKorisnik.setBrojPokusaja(0);
		noviKorisnik.setBrojZaPogadjanje((int)(Math.random() * 150));

		noviKorisnik = korisnikService.save(noviKorisnik);

		return new ResponseEntity<>(noviKorisnik, HttpStatus.CREATED);
	}
	@CrossOrigin
	@RequestMapping(value = "api/korisnici/{id}", method = RequestMethod.GET)
	public ResponseEntity<String> pogadjajBroj(@PathVariable Long id, @RequestParam int broj) {
		Korisnik korisnik = korisnikService.findOne(id);
		if (korisnik == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		String poruka = korisnikService.pogodiBroj(korisnik, broj);

		return new ResponseEntity<>(poruka, HttpStatus.OK);

	}

}
