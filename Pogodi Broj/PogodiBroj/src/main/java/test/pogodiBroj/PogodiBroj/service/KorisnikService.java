package test.pogodiBroj.PogodiBroj.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import test.pogodiBroj.PogodiBroj.model.Korisnik;
import test.pogodiBroj.PogodiBroj.repository.KorisnikRepository;

@Component
public class KorisnikService {

	@Autowired
	KorisnikRepository korisnikRepository;
	
	public List<Korisnik> findAll() {
		return korisnikRepository.findAll();
	}

	public Korisnik findOne(Long id) {
		return korisnikRepository.findOne(id);
	}

	public Korisnik save(Korisnik korisnik) {
		return korisnikRepository.save(korisnik);
	}
	public String pogodiBroj(Korisnik korisnik, int broj) {
		String poruka;
		korisnik.setBrojPokusaja(korisnik.getBrojPokusaja()+1);
		if(broj == korisnik.getBrojZaPogadjanje()) {
			poruka = "Pogodak";
			if(korisnik.getBrojPokusaja()<=5) {
				korisnik.setRang(1);
			}else if(korisnik.getBrojPokusaja()<=10) {
				korisnik.setRang(2);
			}else {
				korisnik.setRang(3);
			}
		}else if(broj < korisnik.getBrojZaPogadjanje()){
			poruka = "Manji";
		}else {
			poruka = "Veci";
		}
		korisnik = this.save(korisnik);
		return poruka;
	}
}
