package test.pogodiBroj.PogodiBroj.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Korisnik {
	@Id
	@GeneratedValue
	private Long id;
	
	private String ime;
	private int rang;
	private int brojPokusaja;
	private int brojZaPogadjanje;
	
	public Korisnik() {
		super();
	}

	public Korisnik(String ime) {
		super();
		this.ime = ime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public int getRang() {
		return rang;
	}

	public void setRang(int rang) {
		this.rang = rang;
	}

	public int getBrojPokusaja() {
		return brojPokusaja;
	}

	public void setBrojPokusaja(int brojPokusaja) {
		this.brojPokusaja = brojPokusaja;
	}

	public int getBrojZaPogadjanje() {
		return brojZaPogadjanje;
	}

	public void setBrojZaPogadjanje(int brojZaPogadjanje) {
		this.brojZaPogadjanje = brojZaPogadjanje;
	}
	
	
}
