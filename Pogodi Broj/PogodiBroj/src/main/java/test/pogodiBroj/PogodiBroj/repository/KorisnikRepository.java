package test.pogodiBroj.PogodiBroj.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import test.pogodiBroj.PogodiBroj.model.Korisnik;

@Component
public interface KorisnikRepository extends JpaRepository<Korisnik, Long>{

}
