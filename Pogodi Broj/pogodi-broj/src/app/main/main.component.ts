import { Component, OnInit } from '@angular/core';
import { KorisnikService } from '../service/korisnik.service';
import { Korisnik } from '../model/korisnik';

@Component({
  selector: 'main-component',
  templateUrl: './main.component.html',
})
export class MainComponent implements OnInit {

korisnici:Korisnik[]=[];
broj:number = 0;
noviKorisnik:Korisnik={
    ime:''
 };
trenutniKorisnik:Korisnik;

  constructor(private korisnikService:KorisnikService) { }

  ngOnInit() {
      this.ucitajKorisnike();
  }
ucitajKorisnike(){
    this.korisnikService.preuzmiKorisnike().subscribe(
        (res)=>{
            this.korisnici = res;
        }
    )
}
sacuvajKorisnika(){
    this.korisnikService.sacuvajKorisnika(this.noviKorisnik).subscribe(
        (res)=>{
            this.trenutniKorisnik = res;
            this.ucitajKorisnike();
        }
    )
}
pogadjajBroj(){
    this.korisnikService.pogadjajBroj(this.broj, this.trenutniKorisnik.id).subscribe(
        (res) => {
            alert(res);
        }
    )
}

}
