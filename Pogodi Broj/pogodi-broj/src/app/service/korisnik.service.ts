import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Korisnik } from '../model/korisnik';

@Injectable()
export class KorisnikService {

  readonly apiUrl: string = 'http://localhost:8080/api/korisnici';

  constructor(private httpClient: HttpClient) { }

  preuzmiKorisnike(): Observable<Korisnik[]> {
    return this.httpClient.get(this.apiUrl) as Observable<Korisnik[]>;
  }
  sacuvajKorisnika(korisnik: Korisnik):Observable<Korisnik>{
      return this.httpClient.post(this.apiUrl, korisnik) as Observable<Korisnik>; 
  }
  pogadjajBroj(broj:number, id:number): Observable<string>{
      return this.httpClient.get(this.apiUrl + `/${id}?broj=${broj}`) as Observable<string>;
  }
}